#!/usr/bin/env python
import os
from bs4 import BeautifulSoup

def askUser():
    page = input("[*] Path to aquatone html report page: ")
    return page

def beautifyHtml(htmlraw):
    soup = BeautifulSoup(htmlraw, 'html.parser')
    return soup

def normalizeLink(a):
    a = a.replace("http://", "")
    a = a.replace("https://","")
    a = a.replace("/","")
    return a

if __name__ == '__main__':
    pageFile = open(askUser(),'r')
    aquatoneReportHtmlRaw = pageFile.read()
    pageFile.close()
    soup = beautifyHtml(aquatoneReportHtmlRaw)

    for linkNameList in soup.find_all(class_='details'):
        linkNameListItems = linkNameList.find('a')
        link = linkNameListItems.get('href')
        link = normalizeLink(link)
        print(link)
        #print(linkNameListItems.get('href'))
